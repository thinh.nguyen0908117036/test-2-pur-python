import psycopg2


def create_table_postgres(query):
    conn = psycopg2.connect(database="test2_database_new",
                            user="admin", password="", host="127.0.0.1", port="5432")
    cur = conn.cursor()
    try:
        cur.execute(query)
        conn.commit()
        print("Đã tạo CSDL ok")
    except:
        print("Database da ton tai")
    finally:
        conn.close()

def create_database_for_test_2():
    query_create_table_list = [
        "CREATE TABLE account (id SERIAL NOT NULL PRIMARY KEY, \
            accountId TEXT UNIQUE, \
                accountType TEXT, \
                    balance NUMERIC  \
                    );",
        "CREATE TABLE merchant (id SERIAL NOT NULL PRIMARY KEY, \
            merchantId TEXT, \
                merchantName TEXT, \
                    accountId TEXT, \
                        apiKey TEXT, \
                            merchantUrl TEXT , \
                                CONSTRAINT merchant_id_unique UNIQUE (merchantId), \
                                    CONSTRAINT account_id_unique UNIQUE (accountId), \
                                        CONSTRAINT fk_account FOREIGN KEY(accountId) REFERENCES account(accountId) \
                        );",
        "CREATE TABLE transaction (id SERIAL NOT NULL PRIMARY KEY, \
            transactionId TEXT, \
                merchantId TEXT, \
                    incomeAccount TEXT, \
                        outcomeAccount TEXT, \
                            amount NUMERIC, \
                                extraData TEXT, \
                                    signature TEXT, \
                                        status TEXT, \
                                            CONSTRAINT transaction_id_unique UNIQUE (transactionId), \
                                                CONSTRAINT fk_merchant FOREIGN KEY(merchantId) REFERENCES merchant(merchantId), \
                                                    CONSTRAINT fk_account FOREIGN KEY(incomeAccount) REFERENCES account(accountId) \
            );"
        ]
    for each_query in query_create_table_list:
        create_table_postgres(each_query)
